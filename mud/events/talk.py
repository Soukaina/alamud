from .event import Event2

class TalkOnEvent(Event2):
    NAME = "Talk-on"

    def perform(self):
        if not self.object.has_prop("talkable"):
            self.fail()
            return self.inform("talk-on.failed")
        self.inform("talk-on")
