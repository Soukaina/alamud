from .action import Action2
from mud.events import TalkOnEvent

class TalkOnAction(Action2):
    EVENT = TalkOnEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "talk-on"
